#include <X11/Xlib.h>
#include <X11/Xresource.h> 	/* xrdb things */
#include <X11/keysym.h>		/* keys */
#include <X11/Xutil.h>		/* vinfo & co */

#include <assert.h>		/* assert */
#include <unistd.h>		/* getopt & co */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sysexits.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define nil NULL

#define SYM_BUF_SIZE 4

struct rendering {
	Display		*d;
	Window		 w;
	Pixmap		 p;
	int		 width;
	int		 height;
	int		 pixmap_width;
	int		 pixmap_height;
	int		 lines;
	int		 line_spacing;
	int		 line_height;
	int		 scrolled_x;
	int		 scrolled_y;
	int		 offset_x;
	XFontSet	 font;
	GC		*bg;
	GC		*text;
	/* GC		 status_bg; */
	/* GC		 status_fg; */
	char		*percentage;
};

/* idea stolen from lemonbar. Thanks, lemonboy! */
typedef union {
	struct {
		uint8_t b;
		uint8_t g;
		uint8_t r;
		uint8_t a;
	};
	uint32_t v;
} rgba_t;

/*
 * Write the width and height of the window `w' respectively in
 * `width' and `height'.
 */
void
get_wh(struct rendering *r)
{
	XWindowAttributes win_attr;
	XGetWindowAttributes(r->d, r->w, &win_attr);
	r->height = win_attr.height;
	r->width  = win_attr.width;
}

/* Compute the dimension of the given string with the given font */
int
text_extents(const char *str, size_t len, XFontSet f, int *ret_width, int *ret_height)
{
	int width, height;
	if (str == nil) {
		width = 0;
		height = 0;
	} else {
		XRectangle rect;
		XmbTextExtents(f, str, len, nil, &rect);
		width = rect.width;
		height = rect.height;
	}
	if (ret_width != nil) *ret_width = width;
	if (ret_height != nil) *ret_height = height;
	return width;
}

/* expects 0 < alpha < 255 */
unsigned long
strtocolor(const char *color, unsigned short alpha, const char *def, Display *d, Colormap colormap)
{
	XColor c, real;
	if (!XAllocNamedColor(d, colormap, color, &c, &real)) {
		fprintf(stderr, "\"%s\" is not a valid color, using \"%s\" as default.",
			color, def);
		XAllocNamedColor(d, colormap, def, &c, &real);
	}

	rgba_t orig = {
		.r = c.red,
		.g = c.green,
		.b = c.green,
		.a = 0xff
	};

	/* the colors should be scaled accordingly to the channel widths. */
	rgba_t ret;
	ret.r = (orig.r * alpha) / 256;
	ret.b = (orig.b * alpha) / 256;
	ret.g = (orig.g * alpha) / 256;
	ret.a = alpha;

	return ret.v;
}

unsigned long
get_alpha(XrmDatabase xdb, char *p, unsigned short def)
{
	char *datatype[20];
	XrmValue value;

	if (XrmGetResource(xdb, p, "*", datatype, &value)) {
		errno = 0;
		char *ep;
		long lval = strtol(value.addr, &ep, 0);
		if ((value.addr[0] == '\0' || *ep != '\0')
		    || (errno == ERANGE && (lval == LONG_MAX || lval == LONG_MIN))
		    || lval < 0 || lval > 256) {
			fprintf(stderr, "%s: \"%s\" is not a valid number or isnt within 0...255. Using 255 as default",
				p, value.addr);
		} else
			def = lval;
	}

	return def;
}

unsigned long
get_color(XrmDatabase xdb, char *p, char *def, unsigned long alpha, Display *d, Colormap c)
{
	char *datatype[20];
	XrmValue value;

	if (XrmGetResource(xdb, p, "*", datatype, &value))
		return strtocolor(value.addr, alpha, def, d, c);
	return strtocolor(def, alpha, def, d, c);
}

int
get_int(XrmDatabase xdb, char *p, int def)
{
	char *datatype[20];
	XrmValue value;

	if (XrmGetResource(xdb, p, "*", datatype, &value)) {
		errno = 0;
		char *ep;
		long lval = strtol(value.addr, &ep, 0);
		if (value.addr[0] == '\0' || *ep != '\0') {
			fprintf(stderr, "%s: \"%s\" is not a number, using %d as default.\n", p, value.addr, def);
			return def;
		}
		if ((errno == ERANGE && (lval == LONG_MAX || lval == LONG_MIN)) || (lval > INT_MAX || lval < INT_MIN)) {
			fprintf(stderr, "%s: \"%s\" is too big, using %d as default.\n", p, value.addr, def);
			return def;
		}
		return lval;
	}

	return def;
}

char *
get_str(XrmDatabase xdb, char *p, char *def)
{
	char *datatype[20];
	XrmValue value;

	if (XrmGetResource(xdb, p, "*", datatype, &value))
		return strdup(value.addr);
	return strdup(def);
}

/* drawing logic */
void
draw(struct rendering *r)
{
	r->scrolled_y = MAX(r->scrolled_y, 0);
	r->scrolled_y = MIN(r->scrolled_y, MAX(r->pixmap_height - r->height, 0));

	r->scrolled_x = MAX(r->scrolled_x, 0);
	r->scrolled_x = MIN(r->scrolled_x, MAX(r->pixmap_width - r->width, 0));

	double psx = r->scrolled_x * 100.0 / r->pixmap_width;
	double psy = r->scrolled_y * 100.0 / r->pixmap_width;
	asprintf(&r->percentage, "%.1f%%, %.1f%% ", psx, psy);
	int percentage_width = 0;
	int percentage_height = 0;
	text_extents(r->percentage, strlen(r->percentage), r->font, &percentage_width, &percentage_height);

	XFillRectangle(r->d, r->w, *r->bg, 0, 0, r->width, r->height);

	int width = MIN(r->width, r->pixmap_width);
	int height = MIN(r->height, r->pixmap_height);
	XCopyArea(r->d, r->p, r->w, *r->text, r->scrolled_x, r->scrolled_y, width, height, r->offset_x, 0);
	XDrawString(r->d, r->w, *r->text, r->width - percentage_width, r->height - percentage_height, r->percentage, strlen(r->percentage));

	XFlush(r->d);
}

/* read from a file into a buffer */
char *
into_buff(FILE *in, size_t *ret_buflen, size_t *ret_lines, size_t *ret_read)
{
	size_t buflen = 1024;
	size_t read = 0;
	size_t lines = 0;
	char *buf = calloc(buflen, sizeof(char));
	if (buf == nil)
		goto memory_allocation_error;

	int c;
	while ((c = fgetc(in)) != EOF) {
		buf[read] = c;
		read++;

		if (c == '\n')
			lines++;

		if (read == (buflen -1)) {
			buflen += buflen >>1;
			char *newbuf = realloc(buf, buflen);
			if (newbuf == nil)
				goto memory_allocation_error;
			buf = newbuf;
			for (size_t i = read; i < buflen; ++i)
				buf[i] = 0;
		}
	}
	lines++;

	*ret_buflen = buflen;
	*ret_lines = lines;
	*ret_read = read;
	return buf;

 memory_allocation_error:
	fprintf(stderr, "Memory allocation failed, was read only a part of the file!\n");
	return buf;
}

/* Main loop logic */
void
evloop(struct rendering *r, XIC xic)
{
	int mouse_last_x = 0;
	int mouse_last_y = 0;
	bool mouse_down  = false;

	int loop = 1;
	while (loop) {

		XEvent e;
		XNextEvent(r->d, &e);

		if (XFilterEvent(&e, r->w))
			continue;

		switch (e.type) {
		case MapNotify:
		case ConfigureNotify:
		case Expose:
			get_wh(r);
			draw(r);
			break;

		case KeymapNotify:
			XRefreshKeyboardMapping(&e.xmapping);
			break;

		case KeyPress: {
			XKeyPressedEvent *ev = (XKeyPressedEvent*)&e;

			if (ev->keycode == XKeysymToKeycode(r->d, XK_Home)) {
				r->scrolled_y = - r->line_height;
				r->scrolled_x = 0;
			} else if (ev->keycode == XKeysymToKeycode(r->d, XK_End)) {
				r->scrolled_y = r->pixmap_height - r->height + r->line_height;
				r->scrolled_x = 0;
			} else if (ev->keycode == XKeysymToKeycode(r->d, XK_Prior)) {
				/* page up */
				r->scrolled_y -= r->height - r->line_height;
			} else if (ev->keycode == XKeysymToKeycode(r->d, XK_Next)) {
				/* page down */
				r->scrolled_y += r->height - r->line_height;
			} else {
				char str[SYM_BUF_SIZE] = {0};
				Status s = 0;
				Xutf8LookupString(xic, ev, str, SYM_BUF_SIZE, 0, &s);
				if (s == XBufferOverflow) {
					/* shouldn't happen, since
					   there are no utf-8 characters
					   larger than 24 bits, but just in case... */
					break;
				}

				switch (str[0]) {
				case 'j': r->scrolled_y += 20; break;
				case 'k': r->scrolled_y -= 20; break;
				case 'l': r->scrolled_x += 20; break;
				case 'h': r->scrolled_x -= 20; break;

				case 'g':
					r->scrolled_x = 0;
					r->scrolled_y = - r->line_height;
					break;

				case 'G':
					r->scrolled_x = 0;
					r->scrolled_y = r->pixmap_height - r->height + r->line_height;
					break;

				case ' ': {
					if (ev->state & ShiftMask)
						r->scrolled_y -= r->width>>1;
					else
						r->scrolled_y += r->width>>1;
					break;
				}
				case 'q': loop = 0; break;
				case 'u': r->scrolled_y -= r->height - r->line_height; break;
				case 'd': r->scrolled_y += r->height - r->line_height; break;
				}
			}

			draw(r);
			break;
		}

		case ButtonPress: {
			XButtonPressedEvent *ev = (XButtonPressedEvent*)&e;
			if (ev->button == Button1) {
				mouse_down = true;
				mouse_last_x = ev->x_root;
				mouse_last_y = ev->y_root;
			}
			break;
		}

		case ButtonRelease: {
			XButtonPressedEvent *ev = (XButtonPressedEvent*)&e;
			if (ev->button == Button1)
				mouse_down = false;
			break;
		}

		case MotionNotify: {
			XPointerMovedEvent *ev = (XPointerMovedEvent*)&e;
			if (mouse_down) {
				int delta_x = ev->x_root - mouse_last_x;
				int delta_y = ev->y_root - mouse_last_y;
				mouse_last_x = ev->x_root;
				mouse_last_y = ev->y_root;

				r->scrolled_x -= delta_x;
				r->scrolled_y -= delta_y;

				draw(r);
			}

			break;
		}
		}
	}
}

int
main(int argc, char **argv)
{
#ifdef HAVE_PLEDGE
	pledge("stdio rpath unix", "");
#endif

	bool embed = false;
	Window parent_window = 0;

	/* parsing command line flag */
	int ch;
	while ((ch = getopt(argc, argv, "e:")) != -1) {
		switch (ch) {
		case 'e':
			embed = true;
			if ((parent_window = (strtol(optarg, nil, 0)))) {
				fprintf(stderr, "Invalid window id \"%s\"\n", optarg);
				embed = false;
			}
			break;

		default:
			fprintf(stderr, "Usage: %s [-e window_id] [file]\n", argv[0]);
			return EX_USAGE;
		}
	}
	argc -= optind;
	argv += optind;

	FILE *in = stdin;
	if (argc == 1) {
		in = fopen(argv[0], "r");
		if (in == nil) {
			perror("Could not open file");
			return EX_USAGE;
		}
	}

	/* reading the file */
	size_t buflen, lines, read;
	char *buf = into_buff(in, &buflen, &lines, &read);

	if (in != stdin)
		fclose(in);

	/* opening connection to xorg */
	Display *d = XOpenDisplay(nil);
	assert(d);

	if (!embed)
		parent_window = DefaultRootWindow(d);

	int width = 500;
	int height = 700;

	/* create window */
	XVisualInfo vinfo;
	XMatchVisualInfo(d, DefaultScreen(d), 32, TrueColor, &vinfo);

	Colormap cmap;
	XSetWindowAttributes attr;
	cmap = XCreateColormap(d, XDefaultRootWindow(d), vinfo.visual, AllocNone);
	attr.colormap = cmap;
	attr.border_pixel = 0;
	attr.background_pixel = 0x80808080;

	Window w = XCreateWindow(d, parent_window, 0, 0, width, height, 0, vinfo.depth, InputOutput,
				 vinfo.visual, CWBackPixel | CWColormap | CWBorderPixel, &attr);

	/* listen to some events */
	XSelectInput(d, w, StructureNotifyMask | ExposureMask | KeyPressMask | KeymapStateMask | PointerMotionMask | ButtonMotionMask | Button1MotionMask | ButtonPressMask | ButtonReleaseMask);
	XMapWindow(d, w);

	GC bg   = XCreateGC(d, w, 0, nil);
	GC text = XCreateGC(d, w, 0, nil);

	int line_spacing = 9;

	char *fontname = nil;

	/* read resources */
	XrmInitialize();
	char *xrm = XResourceManagerString(d);
	XrmDatabase xdb = nil;
	if (xrm != nil) {
		xdb = XrmGetStringDatabase(xrm);

		unsigned short bg_alpha = get_alpha(xdb, "xpg.bg.alpha", 0xff);

		unsigned long bg_color = get_color(xdb, "xpg.bg.background", "black", bg_alpha, d, cmap);
		XSetForeground(d, bg, bg_color);
		XSetBackground(d, bg, bg_color);

		unsigned short text_alpha = get_alpha(xdb, "xpg.text.alpha", 0xff);

		unsigned long text_color = get_color(xdb, "xpg.text.color", "white", text_alpha, d, cmap);
		XSetForeground(d, text, text_color);
		XSetBackground(d, text, bg_color);

		line_spacing = get_int(xdb, "xpg.text.spacing", line_spacing);

		fontname = get_str(xdb, "xpg.text.font", "fixed");
		assert(fontname);
	}

	/* create fontset */
	char **missing_charset_list;
	int missing_charset_count;
	XFontSet font = XCreateFontSet(d, fontname, &missing_charset_list, &missing_charset_count, nil);
	assert(font);
	free(fontname);

	/* Open X Input Method */
	XIM xim = XOpenIM(d, xdb, "xpg", "xpg");
	XIMStyles *xis = nil;
	if (XGetIMValues(xim, XNQueryInputStyle, &xis, nil) || !xis) {
		fprintf(stderr, "Failed to open X Input Method.\n");
		return EX_UNAVAILABLE;
	}

	XIMStyle bestMatchStyle = 0;
	for (int i = 0; i < xis->count_styles; ++i) {
		XIMStyle ts = xis->supported_styles[i];
		if (ts == (XIMPreeditNothing | XIMStatusNothing)) {
			bestMatchStyle = ts;
			break;
		}
	}
	XFree(xis);

	if (!bestMatchStyle)
		fprintf(stderr, "No matching input style could be determined.\n");

	XIC xic = XCreateIC(xim, XNInputStyle, bestMatchStyle, XNClientWindow, w, XNFocusWindow, w, NULL);
	if (xic == nil) {
		fprintf(stderr, "Failed to create X Input Context.\n");
		return EX_UNAVAILABLE;
	}

	int line_height;
	text_extents("abcdefghi", 9, font, nil, &line_height);

	struct rendering r = {
		.d		= d,
		.w		= w,
		.width		= width,
		.height		= height,
		.pixmap_width   = 600, /* TODO: the width should be related to the longest line */
		.pixmap_height  = (line_spacing + line_height) * lines,
		.lines		= lines,
		.line_spacing	= line_spacing,
		.line_height	= line_height,
		.scrolled_x     = 0,
		.scrolled_y     = - line_height,
		.offset_x       = 10,
		.font		= font,
		.bg		= &bg,
		.text		= &text,
		.percentage     = nil
	};

	/* create and draw the pixmap */
	r.p = XCreatePixmap(d, w, r.pixmap_width, r.pixmap_height, vinfo.depth);
	XFillRectangle(d, r.p, *r.bg, 0, 0, r.pixmap_width, r.pixmap_height);

	int x = 0;
	int y = line_spacing <<1;
	char *tok = buf;
	char *end = buf;
	while (tok != nil) { 	/* draw each line */
		strsep(&end, "\n");
		XDrawImageString(d, r.p, *r.text, x, y, tok, strlen(tok));
		tok = end;
		y += line_spacing + line_height;
	}

	/* wait for MapNotifyEvent */
	for (;;) {
		XEvent e;
		XNextEvent(d, &e);
		if (e.type == MapNotify)
			break;
	}

	get_wh(&r);

	draw(&r);

	evloop(&r, xic);

	if (r.percentage != nil)
		free(r.percentage);

	XFreeFontSet(d, font);
	XFreeGC(d, bg);
	XFreeGC(d, text);
	XDestroyWindow(d, w);
	XCloseDisplay(d);

	return 0;
}
