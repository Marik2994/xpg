VERSION	 = 0.1

# You may want to change these:

# not yet:
#OPTIONAL = xft
#CDEFS	+= -DUSE_XFT
CDEFS	 =

# only on OpenBSD
#CDEFS	 += -DHAVE_PLEDGE

#OPTIM	 = -O0 -g -Wall
OPTIM	 = -O3

# You may not want to change these

CC	?= cc
LIBS	 = `pkg-config --libs x11 xft`
CFLAGS	 = `pkg-config --cflags x11 xft` $(CDEFS)

.PHONY: all clean install manpage gnu

all: xpg

xpg: xpg.c
	$(CC) $(CFLAGS) xpg.c -o xpg $(LIBS) $(OPTIM)

manpage: README.md

README.md: xpg.1
	mandoc -T markdown xpg.1 > README.md

clean:
	rm -f xpg

install:
	cp xpg ~/bin

gnu:
	make CDEFS="-D_GNU_SOURCE"
