XPG(1) - General Commands Manual

# NAME

**xpg** - simple page for XOrg

# SYNOPSYS

**xpg**
\[**-e**&nbsp;*windowid*]
\[*file*]

# DESCRIPTION

The
**xpg**
utility is a simple pager for XOrg. It reads some text from the
**stdin**
or from the given file and render a window where that text is rendered.

The following options are available:

**-e** *windowid*

> Embed
> **xpg**
> into another window.

# KEYS

j

> scroll down

k

> scroll up

Space

> Scroll down half-page

Shift-Space

> Scroll up half-page

g

> go to the top

G

> go to the bottom

Home

> go to the top

End

> go to the bottom

Page up

> Scroll one page up

Page down

> Scroll one page down

u

> Scroll one page up

d

> Scroll one page down

q

> exit

# RESOURCES

xpg.bg.alpha

> The alpha channel of the background. A integer (in base 8, 10 or 16)
> within 0..255. Defaults to 255.

xpg.bg.background

> The color of the background. Any thing that xorg can parse as color
> will be fine. Defaults to \`\`black''.

xpg.text.alpha

> Like xpg.bg.alpha but for the text. Defaults to 255

xpg.text.color

> The text color. Defaults to \`\`white''.

xpg.text.spacing

> Inter-line spacing. Any integer (in base 8, 10 or 16) will be
> fine. Defaults to 9.

xpg.text.font

> The font name. Defaults to \`\`fixed''.

OpenBSD 6.3 - August 16, 2018
